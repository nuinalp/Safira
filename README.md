<h2>Safira Sans</h2>

Safira Sans is a family of fonts based on Fira Sans from Mozilla, designed for the Nuinalp Design Language, Plánium OS and other projects.